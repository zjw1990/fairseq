#!/bin/bash
COVO_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/data/covost2/ar
ST_SAVE_DIR=${COVO_ROOT}/st_checkpoints
#FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CHECKPOINT_FILENAME=checkpoint_best.pt
#python setup.py build_ext --inplace

python generate.py ${COVO_ROOT} --gen-subset test_st_ar_en --task speech_to_text \
    --path ${ST_SAVE_DIR}/${CHECKPOINT_FILENAME} --max-tokens 1000 --beam 3 --scoring sacrebleu --config-yaml ${COVO_ROOT}/config_st_ar_en.yaml --cpu