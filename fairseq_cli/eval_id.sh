#!/bin/bash
COVO_ROOT=/mnt/nas/users/qianji.zjw/data/covost2/id
#ST_SAVE_DIR=${COVO_ROOT}/vae_id_st_checkpoints
ST_SAVE_DIR=${COVO_ROOT}/id_st_s2t_reproduce_checkpoints
#FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CHECKPOINT_FILENAME=checkpoint138.pt
#python setup.py build_ext --inplace

python generate.py ${COVO_ROOT} --gen-subset test_st_id_en --task peech_to_text \
    --path ${ST_SAVE_DIR}/${CHECKPOINT_FILENAME} --max-tokens 1000 --beam 3 --scoring sacrebleu --config-yaml ${COVO_ROOT}/config_st_ar_en.yaml --cpu