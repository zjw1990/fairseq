
#!/bin/bash

python setup.py build_ext --inplace

MUSTC_ROOT=/mnt/nas/users/qianji.zjw/data/mustc
ASR_SAVE_DIR=${MUSTC_ROOT}/en-fr/asr_new_checkpoints
FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq

python train.py ${MUSTC_ROOT}/en-fr --train-subset train_asr --valid-subset dev_asr --save-dir ${ASR_SAVE_DIR} \
    --num-workers 0 --max-tokens 20000 --task speech_to_text --criterion label_smoothed_cross_entropy \
    --report-accuracy --max-update 200000 --arch s2t_transformer_s --optimizer adam --lr 1e-3 \
    --lr-scheduler inverse_sqrt --warmup-updates 10000 --clip-norm 10.0 --seed 1 --update-freq 2 --config-yaml ${MUSTC_ROOT}/en-fr/config_asr.yaml --empty-cache-freq 2  --ddp-backend=no_c10d