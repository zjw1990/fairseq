#!/bin/bash
python setup.py build_ext --inplace

MUSTC_ROOT=/mnt/nas/users/qianji.zjw/data/mustc

ST_SAVE_DIR=${MUSTC_ROOT}/en-fr

FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CHECKPOINT_FILENAME=mustc_fr_st_transformer_s.pt

#Evaluate st
fairseq-generate ${MUSTC_ROOT}/en-fr --gen-subset tst-COMMON_st --task speech_to_text --max-tokens 50000 --num-workers 0 \
    --path ${ST_SAVE_DIR}/${CHECKPOINT_FILENAME} --beam 5 --scoring sacrebleu --config-yaml ${MUSTC_ROOT}/en-fr/config_st.yaml --quiet