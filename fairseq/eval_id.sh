#!/bin/bash
COVO_ROOT=/mnt/nas/users/qianji.zjw/data/covost2/id
ST_SAVE_DIR=${COVO_ROOT}/vae_id_st_checkpoints
#FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CHECKPOINT_FILENAME=avg_last_10_checkpoint.pt
#python setup.py build_ext --inplace
#python setup.py build_ext --inplace
python fairseq_cli/generate.py ${COVO_ROOT} --gen-subset test_st_id_en --task task_speech_to_text_vae \
    --path ${ST_SAVE_DIR}/${CHECKPOINT_FILENAME} --max-tokens 1000 --beam 3 --scoring sacrebleu --config-yaml ${COVO_ROOT}/config_st_id_en.yaml