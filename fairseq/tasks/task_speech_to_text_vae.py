# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import logging
import os.path as op
from argparse import Namespace

from fairseq.data import Dictionary, encoders
from fairseq.data.audio.data_speech_to_text_vae import (
    S2TDataConfig,
    SpeechToTextDataset,
    SpeechToTextDatasetCreator,
)
from fairseq.tasks import LegacyFairseqTask, register_task
import torch

logger = logging.getLogger(__name__)


@register_task("task_speech_to_text_vae")
class TaskSpeechToTextVAE(LegacyFairseqTask):
    @staticmethod
    def add_args(parser):
        parser.add_argument("data", help="manifest root path")
        parser.add_argument(
            "--config-yaml",
            type=str,
            default="config.yaml",
            help="Configuration YAML filename (under manifest root)",
        )
        parser.add_argument(
            "--max-source-positions",
            default=6000,
            type=int,
            metavar="N",
            help="max number of tokens in the source sequence",
        )

        parser.add_argument(
            "--max-target-positions",
            default=1024,
            type=int,
            metavar="N",
            help="max number of tokens in the target sequence",
        )

    def __init__(self, args, tgt_dict):
        super().__init__(args)
        #self.src_dict = src_dict
        self.tgt_dict = tgt_dict
        self.data_cfg = S2TDataConfig(op.join(args.data, args.config_yaml))

    @classmethod
    def setup_task(cls, args, **kwargs):
        # read data config
        data_cfg = S2TDataConfig(op.join(args.data, args.config_yaml))
        # read src_dic and tgt_dic
        #src_dict_path = op.join(args.data, data_cfg.src_vocab_filename)  # 'src_spm_char_st_id_en.txt'
        dict_path = op.join(args.data, data_cfg.vocab_filename)  # 'tgt_spm_char_st_id_en.txt'

        # if not op.isfile(src_dict_path):
        #     raise FileNotFoundError(f"Dict not found: {src_dict_path}")
        
        if not op.isfile(dict_path):
            raise FileNotFoundError(f"Dict not found: {dict_path}")
        
        # load dics
        #src_dict = Dictionary.load(src_dict_path) # tgt_dic_path : 'dict.txt'
        tgt_dict = Dictionary.load(dict_path) # src_dic_path : 'dict.txt'
        
        # logger.info(
        #     f"source dictionary size ({data_cfg.src_vocab_filename}): " f"{len(src_dict):,}"
        # )
        logger.info(
            f"target dictionary size ({data_cfg.vocab_filename}): " f"{len(tgt_dict):,}"
        )

        if getattr(args, "train_subset", None) is not None:
            if not all(s.startswith("train") for s in args.train_subset.split(",")):
                raise ValueError('Train splits should be named like "train*".')
        return cls(args, tgt_dict)

    
    def build_criterion(self, args):
        from fairseq import criterions

        if self.data_cfg.prepend_tgt_lang_tag and args.ignore_prefix_size != 1:
            raise ValueError(
                'Please set "--ignore-prefix-size 1" since '
                "target language ID token is prepended as BOS."
            )
        return criterions.build_criterion(args, self)

    def load_dataset(self, split, epoch=1, combine=False, **kwargs):

        is_train_split = split.startswith("train")
        # # build tokenizers for both source and targets languages
        # src_pre_tokenizer = self.build_src_tokenizer(self.args)
        # src_bpe_tokenizer = self.build_src_bpe(self.args)
        
        pre_tokenizer = self.build_tokenizer(self.args)
        bpe_tokenizer = self.build_bpe(self.args)

        self.datasets[split] = SpeechToTextDatasetCreator.from_tsv(
            self.args.data,
            self.data_cfg,
            split,
            self.tgt_dict,
            pre_tokenizer,
            bpe_tokenizer,
            is_train_split=is_train_split,
            epoch=epoch,
            seed=self.args.seed,
        )
    
    # @property
    # def source_dictionary(self):
    #     return self.src_dict
    @property
    def target_dictionary(self):
        return self.tgt_dict

    # define max positions to manage long inputs
    def max_positions(self):
        return self.args.max_source_positions, self.args.max_target_positions

    def build_model(self, args):
        args.input_feat_per_channel = self.data_cfg.input_feat_per_channel
        args.input_channels = self.data_cfg.input_channels
        return super(TaskSpeechToTextVAE, self).build_model(args)

    def build_generator(
        self,
        models,
        args,
        seq_gen_cls=None,
        extra_gen_cls_kwargs=None,
    ):
        if self.data_cfg.prepend_tgt_lang_tag and args.prefix_size != 1:
            raise ValueError(
                'Please set "--prefix-size 1" since '
                "target language ID token is prepended as BOS."
            )
        lang_token_ids = {
            i
            for s, i in self.tgt_dict.indices.items()
            if SpeechToTextDataset.is_lang_tag(s)
        }
        extra_gen_cls_kwargs = {"symbols_to_strip_from_output": lang_token_ids}
        return super().build_generator(
            models, args, seq_gen_cls=None, extra_gen_cls_kwargs=extra_gen_cls_kwargs
        )

    # def build_src_tokenizer(self, args):
    #     logger.info(f"pre-tokenizer: {self.data_cfg.src_pre_tokenizer}")
    #     return encoders.build_tokenizer(Namespace(**self.data_cfg.src_pre_tokenizer))

    # def build_src_bpe(self, args):
    #     logger.info(f"tokenizer: {self.data_cfg.src_bpe_tokenizer}")
    #     return encoders.build_bpe(Namespace(**self.data_cfg.src_bpe_tokenizer))

    def build_tokenizer(self, args):
        logger.info(f"pre-tokenizer: {self.data_cfg.pre_tokenizer}")
        return encoders.build_tokenizer(Namespace(**self.data_cfg.pre_tokenizer))

    def build_bpe(self, args):
        logger.info(f"tokenizer: {self.data_cfg.bpe_tokenizer}")
        return encoders.build_bpe(Namespace(**self.data_cfg.bpe_tokenizer))

    @classmethod
    def build_dataset_for_inference(cls, audio_paths, n_frames):
        return SpeechToTextDataset("interactive", False, {}, audio_paths, n_frames)
    

    def train_step(
        self, sample, model, criterion, optimizer, update_num, ignore_grad=False
    ):
        """
        Do forward and backward, and return the loss as computed by *criterion*
        for the given *model* and *sample*.

        Args:
            sample (dict): the mini-batch. The format is defined by the
                :class:`~fairseq.data.FairseqDataset`.
            model (~fairseq.models.BaseFairseqModel): the model
            criterion (~fairseq.criterions.FairseqCriterion): the criterion
            optimizer (~fairseq.optim.FairseqOptimizer): the optimizer
            update_num (int): the current update
            ignore_grad (bool): multiply loss by 0 if this is set to True

        Returns:
            tuple:
                - the loss
                - the sample size, which is used as the denominator for the
                  gradient
                - logging outputs to display while training
        """
        model.train()
        model.set_num_updates(update_num)
        
        with torch.autograd.profiler.record_function("forward"):

            loss, sample_size, logging_output = criterion(model, sample, update_num)
        
        if ignore_grad:
            loss *= 0
        
        with torch.autograd.profiler.record_function("backward"):
            optimizer.backward(loss)
        
        return loss, sample_size, logging_output


    def valid_step(self, sample, model, criterion, update_num):
        model.eval()
        with torch.no_grad():
            loss, sample_size, logging_output = criterion(model, sample, update_num)
        return loss, sample_size, logging_output
    

    def build_generator(
        self,
        models,
        args,
        seq_gen_cls=None,
        extra_gen_cls_kwargs=None,
    ):
        if self.data_cfg.prepend_tgt_lang_tag and args.prefix_size != 1:
            raise ValueError(
                'Please set "--prefix-size 1" since '
                "target language ID token is prepended as BOS."
            )
        lang_token_ids = {
            i
            for s, i in self.tgt_dict.indices.items()
            if SpeechToTextDataset.is_lang_tag(s)
        }
        extra_gen_cls_kwargs = {"symbols_to_strip_from_output": lang_token_ids}
        return super().build_generator(
            models, args, seq_gen_cls=None, extra_gen_cls_kwargs=extra_gen_cls_kwargs
        )

    def build_tokenizer(self, args):
        logger.info(f"pre-tokenizer: {self.data_cfg.pre_tokenizer}")
        return encoders.build_tokenizer(Namespace(**self.data_cfg.pre_tokenizer))

    def build_bpe(self, args):
        logger.info(f"tokenizer: {self.data_cfg.bpe_tokenizer}")
        return encoders.build_bpe(Namespace(**self.data_cfg.bpe_tokenizer))

    @classmethod
    def build_dataset_for_inference(cls, audio_paths, n_frames):
        return SpeechToTextDataset("interactive", False, {}, audio_paths, n_frames)

    

    