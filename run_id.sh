
# #!/bin/bash

# #python setup.py build_ext --inplace

# COVO_ROOT=/mnt/nas/users/qianji.zjw/data/covost2/id
# #ST_SAVE_DIR=${COVO_ROOT}/s2t_v2_checkpoints
# ST_SAVE_DIR=${COVO_ROOT}/s2t_v1_checkpoints
# FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq

# python train.py ${COVO_ROOT} --train-subset train_st_id_en --valid-subset dev_st_id_en --save-dir ${ST_SAVE_DIR} \
#     --num-workers 0 --max-tokens 10000 --task task_speech_to_text_vae --criterion criterion_speech_to_text_vae \
#     --report-accuracy --max-update 100000 --arch arch_speech_to_text_vae  --optimizer adam --lr 5e-4 \
#     --lr-scheduler inverse_sqrt --warmup-updates 10000 --clip-norm 10.0 --seed 1 --config-yaml ${COVO_ROOT}/config_st_ar_en.yaml


#####################################################


#!/bin/bash

python setup.py build_ext --inplace

COVO_ROOT=/mnt/nas/users/qianji.zjw/data/covost2/es
ST_SAVE_DIR=${COVO_ROOT}/s2t_base_es
FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq


python train.py ${COVO_ROOT} --train-subset train_st_es_en --valid-subset dev_st_es_en --save-dir ${ST_SAVE_DIR} \
    --num-workers 0 --max-tokens 10000 --task speech_to_text --criterion label_smoothed_cross_entropy \
    --max-update 100000 --arch s2t_transformer_s  --optimizer adam --lr 5e-4 \
    --lr-scheduler inverse_sqrt --warmup-updates 10000 --clip-norm 10.0 --seed 1 --config-yaml ${COVO_ROOT}/config_st_es_en.yaml

