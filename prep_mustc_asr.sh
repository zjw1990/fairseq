#!/bin/bash

MUSTC_ROOT=/mnt/nas/users/qianji.zjw/data/mustc
FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq

python examples/speech_to_text/prep_mustc_data.py --data-root ${MUSTC_ROOT} --task asr \
    --vocab-type unigram --vocab-size 5000