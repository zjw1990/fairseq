#!/bin/bash

#python setup.py build_ext --inplace
MUSTC_ROOT=/mnt/nas/users/qianji.zjw/data/mustc
TEST_VAE_ST_SAVE_DIR=${MUSTC_ROOT}/en-fr/random_st_avae_checkpoints_2
FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
ASR_SAVE_DIR=${MUSTC_ROOT}/en-fr/asr_new_checkpoints
ASR_CHECKPOINT_FILENAME=mustc_fr_asr_transformer_s.pt

python train.py ${MUSTC_ROOT}/en-fr --train-subset train_small_rand --valid-subset dev_st --save-dir ${TEST_VAE_ST_SAVE_DIR} \
    --max-tokens 10000 --task task_speech_to_text_vae --criterion criterion_speech_to_text_vae --num-workers 0 \
    --report-accuracy --max-update 200000 --arch arch_speech_to_text_vae  --optimizer adam --lr 2e-4 \
    --lr-scheduler inverse_sqrt --warmup-updates 10000 --clip-norm 10.0 --seed 1 --config-yaml ${MUSTC_ROOT}/en-fr/config_st.yaml \
    --empty-cache-freq 1 --ddp-backend=no_c10d --load-pretrained-encoder-from ${ASR_SAVE_DIR}/${ASR_CHECKPOINT_FILENAME}
