#!/bin/bash
# python setup.py build_ext --inplace
MUSTC_ROOT=/mnt/nas/users/qianji.zjw/data/mustc
ASR_SAVE_DIR=${MUSTC_ROOT}/en-fr/asr_new_checkpoints
FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CHECKPOINT_FILENAME=mustc_fr_asr_transformer_s.pt


# python scripts/average_checkpoints.py --inputs ${ST_SAVE_DIR} --num-epoch-checkpoints 5 \
#     --output "${ST_SAVE_DIR}/${CHECKPOINT_FILENAME}"


# Evaluate ASR
fairseq-generate ${MUSTC_ROOT}/en-fr --gen-subset tst-COMMON_asr --task speech_to_text \
    --path ${ASR_SAVE_DIR}/${CHECKPOINT_FILENAME} --max-tokens 50000 --beam 5 --results-path ${FAIR_ROOT} \
    --scoring wer --wer-tokenizer 13a --wer-lowercase --wer-remove-punct --config-yaml ${MUSTC_ROOT}/en-fr/config_asr.yaml --num-workers 0 \
    --tokenizer moses
