#!/bin/bash
COVO_ROOT=/mnt/nas/users/qianji.zjw/data/covost2/id
#ST_SAVE_DIR=${COVO_ROOT}/vae_id_st_checkpoints
ST_SAVE_DIR=${COVO_ROOT}/s2t_v1_checkpoints
#FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CHECKPOINT_FILENAME=avg_last_10_checkpoint.pt
python scripts/average_checkpoints.py --inputs ${ST_SAVE_DIR} --num-epoch-checkpoints 10 \
    --output "${ST_SAVE_DIR}/${CHECKPOINT_FILENAME}"
python generate.py ${COVO_ROOT} --gen-subset test_st_id_en --task task_speech_to_text_vae --batch-size 5 \
    --path ${ST_SAVE_DIR}/${CHECKPOINT_FILENAME} --beam 5 --scoring sacrebleu --config-yaml ${COVO_ROOT}/config_st_id_en.yaml