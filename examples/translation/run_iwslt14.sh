#!/usr/bin/env bash

DATA_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
CUDA_VISIBLE_DEVICES=0 fairseq-train ${DATA_ROOT}/data-bin/iwslt14.tokenized.de-en \
    --arch transformer_mt_iwslt_de_en \
    --optimizer adam --adam-betas '(0.9, 0.98)' --clip-norm 0.0 \
    --lr 5e-4 --lr-scheduler inverse_sqrt --warmup-updates 1000 \
    --dropout 0.3 --weight-decay 0.0001 \
    --criterion label_smoothed_cross_entropy --label-smoothing 0.1 \
    --max-tokens 1024 \
    --eval-bleu \
    --eval-bleu-args '{"beam": 5, "max_len_a": 1.2, "max_len_b": 10}' \
    --eval-bleu-detok moses \
    --eval-bleu-remove-bpe \
    --eval-bleu-print-samples \
    --best-checkpoint-metric bleu --maximize-best-checkpoint-metric \