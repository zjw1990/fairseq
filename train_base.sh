#!/bin/bash

#python setup.py build_ext --inplace
MUSTC_ROOT=/mnt/nas/users/qianji.zjw/data/mustc
TEST_BASE_ST_SAVE_DIR=${MUSTC_ROOT}/en-fr/kd_base_checkpoints_5
FAIR_ROOT=/mnt/nas/users/qianji.zjw/workspace/gitlab.alibaba-inc.com/qianji.zjw/testproject/fairseq
ASR_SAVE_DIR=${MUSTC_ROOT}/en-fr/asr_new_checkpoints
ASR_CHECKPOINT_FILENAME=mustc_fr_asr_transformer_s.pt

python train.py ${MUSTC_ROOT}/en-fr --train-subset train_kd_base --valid-subset dev_st --save-dir ${TEST_BASE_ST_SAVE_DIR} \
    --max-tokens 10000 --task speech_to_text --criterion label_smoothed_cross_entropy --num-workers 0 \
    --max-update 300000 --arch s2t_transformer_s --optimizer adam --lr 5e-4 \
    --lr-scheduler inverse_sqrt --warmup-updates 10000 --clip-norm 10.0 --seed 1 --config-yaml ${MUSTC_ROOT}/en-fr/config_st.yaml \
    --empty-cache-freq 2 --report-accuracy --load-pretrained-encoder-from ${ASR_SAVE_DIR}/${ASR_CHECKPOINT_FILENAME} --update-freq 2